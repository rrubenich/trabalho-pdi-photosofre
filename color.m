% function im = color(level, color, image)
%     color = color * 255;
%     
%     [m,n,x] = size(image);
%     ec = uint8(zeros(m,n,x)); 
%     im = image;
% 
%     for i = 0 : m
%         for j = 0 : n
%             
%            r = image(i,j,1);
%            g = image(i,j,2);
%            b = image(i,j,3);
%            d =  ((double(r-color(1))^2) + (double(g-color(2))^2) + (double(b-color(3))^2));
%            
%            dx = sqrt(d)
% 
%            if(dx < level)
%                 ec(i,j,:)-image(i,j,:);
%            end
%         end
%     end
% end

function im = color(x, color, image)
    color = color * 255;
    im = image;
    row = size(im,1);
    columns = size(im,2);
    
    
    for i=1:row
        for j=1:columns
            r = im(i,j,1);
            g = im(i,j,2);
            b = im(i,j,3);
            d = sqrt((double(r-color(1))^2) + (double(g-color(2))^2) + (double(b-color(3))^2));
            if(d > x) 
                im(i,j,:)=0;
            end
        end
    end
end