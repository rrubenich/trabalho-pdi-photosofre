function image = sobel( image )
    img = rgb2gray(image);
    image = edge(img, 'Sobel');
end

