function image = mathUnion(imageLeft, imageRight)
    image1 = im2bw(imageLeft);
    image2 = im2bw(imageRight);
    
    image = image1|image2;
end

