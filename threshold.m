function im = threshold (level, image)
    img = rgb2gray(image);
    threshold = graythresh(img);
    im = im2bw(img,threshold);
end