function image = mathCompl(imageLeft)
    image1 = im2bw(imageLeft);
    
    image = ~image1;
end