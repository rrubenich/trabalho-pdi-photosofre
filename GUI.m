function varargout = GUI(varargin)
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @GUI_OpeningFcn, ...
                       'gui_OutputFcn',  @GUI_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
 
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end

function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
    handles.output = hObject;
    handles.images = [];
    handles.his = [];
    
    guidata(hObject, handles);
    
    set(handles.image1,'xtick',[])
    set(handles.image1,'ytick',[])
    set(handles.image2,'xtick',[])
    set(handles.image2,'ytick',[])

function varargout = GUI_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles.output;

function path2_Callback(hObject, eventdata, handles)

function path2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function path1_Callback(hObject, eventdata, handles)

function path1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function features_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

function level_Callback(hObject, eventdata, handles)

function level_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%Estratégia encontrada pelo jonathan para imprimir a transformada
function imprimirTransformadaHough(imagem, lines, handles)
    axes(handles.image2);
    imshow(imagem), hold on
    max_len = 0;
    for k = 1:length(lines)
       xy = [lines(k).point1; lines(k).point2];
       plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');

       % Plot beginnings and ends of lines
       plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
       plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');

       % Determine the endpoints of the longest line segment
       len = norm(lines(k).point1 - lines(k).point2);
       if ( len > max_len)
          max_len = len;
          xy_long = xy;
       end
    end

    % highlight the longest line segment
    plot(xy_long(:,1),xy_long(:,2),'LineWidth',2,'Color','red');
    hold off


% Botões da app
% Botão da imagem 1
function pushbutton1_Callback(hObject, eventdata, handles)
    try
        path = get(handles.path1,'String');
        imshow(path, 'Parent' , handles.image1);
        image = imread(path);
        handles.images{1} = image;
        handles.his{1} = image;

        guidata(hObject, handles);
    catch
        warndlg('Imagem nao existe!','Atenção')
    end

function features_Callback(hObject, eventdata, handles)

% Botão da imagem 2
function pushbutton3_Callback(hObject, eventdata, handles)
    try
        path = get(handles.path2,'String');
        imshow(path, 'Parent', handles.image2);
        image = imread(path);
        handles.images{2} = image;
        handles.his{2} = image;

        guidata(hObject, handles);
    catch
        warndlg('Imagem nao existe!','Atenção')
    end

function aplicar_Callback(hObject, eventdata, handles)
    filter = get(handles.features, 'Value');
    level  = str2double(get(handles.level, 'String'));
    images_len = length(handles.images);

    if (isnan(level))
        level = 1;
    end
    
    if(level > 255)
        level = 255;
    end

    if(images_len < 1)
        warndlg('Carregue uma imagem!','Atenção')

    elseif(images_len == 1)
        switch filter
            case 1
                disp('Passa Alta')
                handles.images{2} = highPass(level, handles.images{1});
            case 2
                disp('Média')  
                handles.images{2} = average(level, handles.images{1});      
            case 3
                disp('Mediana')       
                handles.images{2} = median(level, handles.images{1});    
            case 4
                disp('Threshoking')   
                handles.images{2} = threshold(level, handles.images{1});      
            case 5
                disp('Sobel')
                handles.images{2} = sobel(handles.images{1});    
            case 6
                disp('Transformada de Hough')
                lines = houghTransform(handles.images{1});
                imprimirTransformadaHough(handles.images{1}, lines, handles);
            case 7
                disp('Seam Carving') 
                handles.images{2} = seamCarving(handles.images{1}, level);   
            case 8
                x = uisetcolor;
                disp('Extração de Cor') 
                handles.images{2} = color(level, x, handles.images{1});        
            case 9
                disp('Intersecção')
                warndlg('Carregue a segunda imagem!','Atenção')
            case 10
                disp('União')  
                warndlg('Carregue a segunda imagem!','Atenção')      
            case 11
                disp('Diferença')
                warndlg('Carregue a segunda imagem!','Atenção')      
            case 12
                disp('Complemento')
                warndlg('Carregue a segunda imagem!','Atenção') 
            case 13
                disp('Salt & Pepper')
                handles.images{2} = imnoise(handles.images{1},'salt & pepper');
        end
        axes(handles.image2);
        handles.his{2} = handles.images{1};
        imshow(handles.images{2});
        
    elseif(images_len > 1)
        switch filter
            case 1
                disp('Passa Alta')
                handles.images{1} = highPass(level, handles.images{1});
                handles.images{2} = highPass(level, handles.images{2});
            case 2
                disp('Média')      
                handles.images{1} = average(level, handles.images{1});
                handles.images{2} = average(level, handles.images{2});  
            case 3
                disp('Mediana')
                handles.images{1} = median(level, handles.images{1});
                handles.images{2} = median(level, handles.images{2});  
            case 4
                disp('Threshoking')
                handles.images{1} = threshold(level, handles.images{1});
                handles.images{2} = threshold(level, handles.images{2});    
            case 5
                disp('Sobel')
                handles.images{1} = edge(handles.images{1}, 'Sobel');
                handles.images{2} = edge(handles.images{2}, 'Sobel');     
            case 6
                disp('Transformada de Hough')
                lines = houghTransform(handles.images{1});
                imprimirTransformadaHough(handles.images{1}, lines, handles);
            case 7
                disp('Seam Carving') 
                handles.images{1} = seamCarving(handles.images{1}, level); 
                handles.images{2} = seamCarving(handles.images{2}, level);      
            case 8
                x = uisetcolor;
                disp('Extração de Cor') 
                handles.images{2} = color(level, x, handles.images{1});    
            case 9
                disp('Intersecção') 
                handles.images{2} = mathIntersection(handles.images{1}, handles.images{2}); 
            case 10
                disp('União')  
                handles.images{2} = mathUnion(handles.images{1}, handles.images{2});    
            case 11
                disp('Diferença')
                handles.images{2} = mathDiff(handles.images{1}, handles.images{2});        
            case 12
                disp('Complemento')
                handles.images{2} = mathCompl(handles.images{1}); 
            case 13
                disp('Salt & Pepper')
                handles.images{1} = imnoise(handles.images{1},'salt & pepper');
                handles.images{2} = imnoise(handles.images{2},'salt & pepper');
        end
        axes(handles.image1);
        imshow(handles.images{1});
        axes(handles.image2);
        imshow(handles.images{2});
    else
        
    end
    
    guidata(hObject,handles);
    
    
    
function back2_Callback(hObject, eventdata, handles)
    axes(handles.image2);
    handles.images{2} = handles.his{2};
    imshow(handles.his{2});
    guidata(hObject,handles);

function back1_Callback(hObject, eventdata, handles)
    axes(handles.image1);
    handles.images{1} = handles.his{1};
    imshow(handles.his{1});
    guidata(hObject,handles);
