function im = average (level,image)
    filter = fspecial('average',[level, level]);
    image = imfilter(image, filter);
    im = image;
end