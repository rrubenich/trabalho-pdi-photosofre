function lines = houghTransform (image)
    % Convert to intensity.
    I  = rgb2gray(image);

    % Extract edges.
    BW = edge(I,'canny');
    [H,T,R] = hough(BW,'RhoResolution',0.5,'Theta',-90:0.5:89.5);

    P = houghpeaks(H,5,'threshold',ceil(0.3*max(H(:))));
    lines = houghlines(BW,T,R,P,'FillGap',5,'MinLength',1);
end
