function im = highPass (level, image)
    meio = ceil(level/2);
    for i=1:level
        for j=1:level
            if (i == meio && j == meio)
                mask(i,j)=level*level-1;
            else
                mask(i,j)=-1;
            end
        end
    end
    
    if (ndims(image)==3)
        gray = rgb2gray(image);
    else
        gray = image;
    end
    im = imfilter(gray,mask);
end