function im = median (level,image)
    for c = 1 : 3
        image(:, :, c) = medfilt2(image(:, :, c), [level, level]);
    end
    im = image;
end